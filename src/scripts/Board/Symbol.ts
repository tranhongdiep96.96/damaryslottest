
import * as PIXI from 'pixi.js';
import { MainApp } from '../app';
import { GameScene } from '../GameScene';
import * as Utils from '../utils';
import SlotBoard from './Board';



export default class SlotSymbol {

    private _resultTexture: PIXI.Texture;
    private _sprite: PIXI.Sprite;
    private _currentType: string;
    private _symbolID : string;
    private _isFake:boolean;

    private _isEnding:boolean = false;
    private _isRunning:boolean = false;
    
    private _startPos: number = 0;
    private _endPos: number = 0;
    private _instPos: number = 0;
    private _blurFilter: PIXI.filters.BlurFilter;

    private _runSpeed:number=17;

    private _callbacks:Function[]=[];

    private _slotBoard: SlotBoard;

    constructor(symbolID: string, isFake:boolean, sprite: PIXI.Sprite, posYStart:number, posYEnd:number, slotBoard: SlotBoard){
        this._symbolID = symbolID;
        this._sprite = sprite;
        this._isFake = isFake;
        this._endPos = posYEnd;
        this._startPos = posYStart;
        this._instPos = sprite.position.y;
        this._slotBoard = slotBoard;
        this._blurFilter = new PIXI.filters.BlurFilter();
        this._sprite.filters = [this._blurFilter];
        this._blurFilter.blur =0;
    }
    public addEventCallback(callback: Function){ if(callback) this._callbacks.push(callback);}

    //sprite funtions
    public changeSprite(texture: PIXI.Texture){ this._sprite.texture = texture; }
    public resetSpritePos(){ this._sprite.position.y= this._instPos; }
    public unBlurSymbol(){ this._blurFilter.blurY = 0; }
    public updateBlur( speed:number){this._blurFilter.blurY = speed/4;}

    public checkReadyForEnd(){ return this._sprite.position.y>this._instPos+GameScene.SYMBOL_HEIGHT/2;}
    public isRunning():boolean{ return this._isRunning }
    public setResultTexture(texture:PIXI.Texture){this._resultTexture = texture;}

    public startRun(): void{
        this._isRunning = true;
        Utils.tweenTo(this._sprite,this._sprite.position, this._endPos,null, this._runSpeed, Utils.backout(0.5),this.updateBlur(1), ()=>this.loop(this._runSpeed));
    }
    public endRun(): void{ this._isEnding=true; }

    private loop(step:number): void{
        this.resetSprite(this._isEnding);
        if(!this._isEnding)
            Utils.tweenTo(this._sprite,this._sprite.position, this._endPos,null, step, Utils.backout(0.5),this.updateBlur(step),()=>this.loop(step));
        else{
            Utils.tweenTo(this._sprite,this._sprite.position, this._instPos,null, step, Utils.backout(0.5),this.updateBlur(1),()=>this.settupEndRun());
        }
    }
    private settupEndRun(){
        console.log('settupEndRun',this._symbolID);
        this._isEnding=false;
        this._isRunning = false;
        this.unBlurSymbol();
        this._callbacks.forEach(callback => {
            callback();
        });
    }
    private resetSprite(isEnding:boolean=false){
        if(isEnding){//if ending, get result texture from _resultTexture
            //this._sprite.position.y= this._instPos- GameScene.SYMBOL_HEIGHT*(GameScene.NUMBER_OF_ROWS-1);
            this._sprite.position.y= this._startPos;
            this.changeSprite(this._resultTexture);
        }
        else{//if in waiting, get a random texture for show
            //this._sprite.position.y= this._instPos- GameScene.SYMBOL_HEIGHT*(GameScene.NUMBER_OF_ROWS-1);
            this._sprite.position.y= this._startPos;
            this.changeSprite(this._slotBoard.getRandomSymbolTexutre());
        }
    }

}