import { GameScene } from "../GameScene";
import SlotReel from "../Board/Reel";
import SlotSymbol from '../Board/Symbol'
import { MainApp } from '../app';

import * as PIXI from 'pixi.js';
import * as Utils from '../utils';

export default class SlotBoard {

    private _reels: SlotReel[] = [];
    private _reelNumber: number;
    private _rowsNumber: number;
    private _gamescene: GameScene;
    private _symbolTextures: Object;

    private _boardRoot: PIXI.Point = new PIXI.Point(MainApp.W_CENTER, MainApp.H_CENTER + 100);
    private _posYEnd: number = 0;
    private _posYStart: number =0;
    private _margin:number = 5;

    private _reelContainers: PIXI.Container[]=[];

    private _boardMask : PIXI.Graphics;

    private _timeToStartRunning : number=0;
    private _timeWaitToRun: number = 0.5;
    private _minTimeToRun:number = this._timeWaitToRun*GameScene.NUMBER_OF_REELS; 

    private _isRunning : boolean = false;


    private defaultBoard = ['2', '7', '3', '4', '6', '8', '7', '3', 'K', '1', '5', '3', '4', '5', '6'];

    constructor(gamescene: GameScene, reelNumber:number , rowsNumber: number, symbolTextures: Object){

        this._gamescene = gamescene;
        this._symbolTextures = symbolTextures;

        this._reelNumber = reelNumber;
        this._rowsNumber = rowsNumber;

        const boardContainer = new PIXI.Container();
        boardContainer.position = this._boardRoot;
        this._gamescene.addChild(boardContainer);

        //set position of start and end for symbol loop -- feel free for change fakeSymbolLength
        const fakeSymbolLength = 1;
        this._posYStart = -fakeSymbolLength * GameScene.SYMBOL_HEIGHT - this._gamescene.boardHeight / 2;
        this._posYEnd = GameScene.NUMBER_OF_ROWS*GameScene.SYMBOL_HEIGHT - this._gamescene.boardHeight / 2;


        //update tween
        Utils.updateTwene();

        //create border for board
        this.createBorder();

        //create mask for board
        this.createMask();

        //create board, inclue reels, symbols
        for(let reelIndex=0;reelIndex<this._reelNumber;reelIndex++){
            let reelSymbol = [];
            let fakeSymbol = [];

            const reelContainer = this._gamescene.addChild(new PIXI.Container());
            reelContainer.position = new PIXI.Point(MainApp.W_CENTER, this._boardRoot.y);

            for(let symbolIndex=0;symbolIndex<GameScene.NUMBER_OF_ROWS;symbolIndex++){
                reelSymbol.push(this.CreateSymbol(reelIndex,symbolIndex,reelContainer));
            }
            for(let symbolIndex=-1;symbolIndex>-fakeSymbolLength-1;symbolIndex--){
                fakeSymbol.push(this.CreateSymbol(reelIndex,symbolIndex,reelContainer));
            }

            this._reels.push(new SlotReel(reelIndex.toString(), reelSymbol, fakeSymbol, reelContainer));
            this._reelContainers.push(reelContainer);
        }
    }
    public startSpin(){
        console.log('========== start spin ==========');
        this._reels.forEach((reel, index)=>{
            reel.startRun(index);
        });
        this._timeToStartRunning = Date.now();
        this._isRunning = true;
    }
    public endSpin(board: any, symbolTextures: any, callback: any){
        if(this._isRunning){
            let timeLeft = this._minTimeToRun*1000 - (Date.now()- this._timeToStartRunning);
            //make sure all reel is started
            if(timeLeft>=0){
                setTimeout(() => {
                    this.setupEndRun(symbolTextures, board, callback);
                }, timeLeft );
            }
            else{
                this.setupEndRun(symbolTextures, board, callback);
            }
        }
    }
    public setupEndRun(symbolTextures: PIXI.Texture[], board: string[], callback: any) { 
        this._reels.forEach((reel, index)=>{
            let reelTextures = [];
            let typeIndex = index * GameScene.NUMBER_OF_ROWS;
            for(let i=0;i<GameScene.NUMBER_OF_ROWS;i++){
                reelTextures.push(symbolTextures[board[typeIndex+i]]);
            }
            setTimeout(() => {
                if(index==GameScene.NUMBER_OF_REELS-1 )//reel GameScene.NUMBER_OF_REELS-1 is a last reel end
                    reel.endRun(reelTextures,callback);
                else{
                    reel.endRun(reelTextures,null);
                }
            }, index*500);
        });
    }
    private createBorder(){
        var bordBorder = new PIXI.Graphics();
        bordBorder.lineStyle(5, 0xFF0000);
        bordBorder.drawRect(0+this._margin, this._boardRoot.y-this._gamescene.boardHeight/2-(150/2)-this._margin, this._gamescene.boardWidth+this._margin, this._gamescene.boardHeight+this._margin);
        console.log(this._boardRoot.y,this._gamescene.boardHeight);
        this._gamescene.addChild(bordBorder)
    }
    private createMask(){
        const graphics = new PIXI.Graphics();
        graphics.beginFill(0xFF3300);
        graphics.drawRect(0+this._margin, this._boardRoot.y-this._gamescene.boardHeight/2-(150/2), this._gamescene.boardWidth+this._margin, this._gamescene.boardHeight);
        graphics.endFill();
        this._gamescene.addChild(graphics);
        this._boardMask = graphics;
    }
    public getRandomSymbolTexutre(){
        return this._gamescene.getRandomSymbolTexture();
    }
    public getRandomSymbolBlurTexture(){
        return this._gamescene.getRandomSymbolBlurTexture();
    }
    public CreateSymbol(reelIndex: number, symbolIndex:number, parent: PIXI.Container): SlotSymbol{
        const pos = new PIXI.Point(reelIndex * GameScene.SYMBOL_WIDTH - this._gamescene.boardWidth / 2 + GameScene.SYMBOL_WIDTH / 2, symbolIndex * GameScene.SYMBOL_HEIGHT - this._gamescene.boardHeight / 2);
        const symbolSpr = new PIXI.Sprite(this._symbolTextures[this.defaultBoard[Utils.getRandomInt(this.defaultBoard.length)]]);
        symbolSpr.position = pos;
        symbolSpr.anchor.set(0.5);
        parent.addChild(symbolSpr);
        symbolSpr.mask=this._boardMask;
        return new SlotSymbol(reelIndex.toString()+'_'+symbolIndex.toString(), false, symbolSpr,this._posYStart, this._posYEnd, this);
    }
}