import SlotSymbol from '../Board/Symbol';
import { GameScene } from '../GameScene';
import { MainApp } from '../app';
import * as Utils from '../utils';


export default class SlotReel {
    static TIME_DELAY_FOR_START = 500;

    private _reelSymbols : SlotSymbol[] = [];
    private _fakeSymbols : SlotSymbol[] = [];
    private _symbols : SlotSymbol[] = [];

    private _timeWaitToRun: number = 0.5;

    private _isEnding: boolean =false;
    private _isRunning: boolean =false;
    private _isReadyForEnd: boolean =false;

    private _callbacks: Function[]= [];
    private _reelID : string;
    
    private _reelContainer : PIXI.Container;

    constructor(reelID: string, reelsymbols: SlotSymbol[], fakeSymbol: SlotSymbol[], container: PIXI.Container){
        this._reelID = reelID;
        this._reelSymbols = reelsymbols;
        this._fakeSymbols = fakeSymbol;
        this._symbols = this._reelSymbols.concat(this._fakeSymbols);
        this._reelContainer = container;
        MainApp.inst.app.ticker.add(this.onUpdate, this);
    }
    public startRun(delay: number){
        //start with delay time
        this._timeWaitToRun = delay* SlotReel.TIME_DELAY_FOR_START;
        setTimeout(() => {
            this._symbols.forEach(symbol => {
                symbol.startRun();
            });
        }, this._timeWaitToRun);
        this._isRunning =true;
    }
    public endRun(reelTextures: PIXI.Texture[],callback:Function){
        this._isEnding =true;
        this._symbols.forEach((symbol,index) => {
            symbol.setResultTexture(reelTextures[index] || reelTextures[Utils.getRandomInt(reelTextures.length-1)]);//the last loop with take texture for show result
            //symbol.addEventCallback(callback);
            //symbol.endRun();
        });
        if(callback){
            this._symbols[GameScene.NUMBER_OF_ROWS-1].addEventCallback(callback);
        }
        this._symbols[GameScene.NUMBER_OF_ROWS-1].addEventCallback(()=>this.setupEndRun());
    }
    private setupEndRun(){
        this._isRunning = false;
        this._isEnding = false;
        this._isReadyForEnd = false;
        this._callbacks.forEach(callback => {
            callback();
        });
    }
    public onUpdate (dtScalar: number) {
        if(this._isEnding){
            if(this._isReadyForEnd== false){
                if(this._symbols[GameScene.NUMBER_OF_ROWS-1].checkReadyForEnd()){// check the last row is symbols[2]
                    this._isReadyForEnd=true;
                    this._symbols.forEach((symbol,index) => {
                        symbol.endRun();
                    });
                }
            }
        }
    }
}