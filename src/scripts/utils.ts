import { MainApp } from "./app";

export function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
export function lerp(a1, a2, t) {
    return a1 * (1 - t) + a2 * t;
}
export function backout(amount) {
    return (t) => (--t * t * ((amount + 1) * t + amount) + 1);
}

// Very simple tweening utility function. This should be replaced with a proper tweening library in a real product.
const tweening = [];
export function tweenTo(object, property, target, time, step, easing, onchange, oncomplete) {
    const tween = {
        object,
        property,
        propertyBeginValue: property.y,
        target,
        easing,
        step,
        time,
        change: onchange,
        complete: oncomplete,
        start: Date.now(),
    };
    //console.log('tween',tween);
    tweening.push(tween);
    return tween;
}
// Listen for animate update.
export function updateTwene(){
    MainApp.inst.app.ticker.add((delta) => {
        const remove = [];
        const now = Date.now();
        for (let i = 0; i < tweening.length; i++) {
            const t = tweening[i];

            const phase = Math.min(1, (now - t.start) / t.time);
            //console.log(phase);
            if(t.time){
                //console.log(t.target);
                t.object.position.y = lerp(t.propertyBeginValue, t.target, t.easing(phase));
                if (t.change) t.change(t);
                if (phase === 1) {
                    t.object[t.property] = t.target;
                    if (t.complete) t.complete(t);
                    remove.push(t);
                }
            }
            else{
                t.object.position.y +=t.step;
                if (t.object.position.y>t.target) {
                    t.object.position.y = t.target;
                    if (t.complete) t.complete(t);
                    remove.push(t);
                }
            }


        }
        for (let i = 0; i < remove.length; i++) {
            tweening.splice(tweening.indexOf(remove[i]), 1);
        }
    });
}