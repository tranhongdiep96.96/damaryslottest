
import * as PIXI from 'pixi.js';
import { MainApp } from './app';
import Server from './Server';
import SlotBoard from './Board/Board';
import * as Utils from './utils';



const symbolTextures = {
    // '1' : null,
    // '2' : null,
    // '3' : null,
    // '4' : null,
    // '5' : null,
    // '6' : null,
    // '7' : null,
    // '8' : null,
    // 'K' : null
};
const symbolBlurTextures = {
    // '1' : null,
    // '2' : null,
    // '3' : null,
    // '4' : null,
    // '5' : null,
    // '6' : null,
    // '7' : null,
    // '8' : null,
    // 'K' : null
};

const symbolTypes = ['1', '2', '3', '4', '5', '6', '7', '8', 'K'];

export class GameScene extends PIXI.Container {
    constructor (server: Server) {
        super();

        /**
         * Register spin data responded event handler
         */
        this._server = server;
        this._server.registerDataRespondEvent(this._onSpinDataResponded.bind(this));

        /**
         * Added onUpdate function to PIXI Ticker so it will be called every frame
         */
        MainApp.inst.app.ticker.add(this.onUpdate, this);
        /**
         * Ask PIXI Loader to load needed resources
         */
        MainApp.inst.app.loader
            .add('logo', 'images/logo.png')
            .add('symbol_1', 'images/symbol_1.png')
            .add('symbol_2', 'images/symbol_2.png')
            .add('symbol_3', 'images/symbol_3.png')
            .add('symbol_4', 'images/symbol_4.png')
            .add('symbol_5', 'images/symbol_5.png')
            .add('symbol_6', 'images/symbol_6.png')
            .add('symbol_7', 'images/symbol_7.png')
            .add('symbol_8', 'images/symbol_8.png')
            .add('symbol_K', 'images/symbol_K.png')
            .load(this._onAssetsLoaded.bind(this)); 
    }

    static readonly NUMBER_OF_REELS = 5;
    static readonly NUMBER_OF_ROWS = 3;
    static readonly SYMBOL_WIDTH = 140;
    static readonly SYMBOL_HEIGHT = 150;

    private _server: Server;
    private _slotBoard: SlotBoard;

    private _isInitialized: boolean = false;
    private _logoSprite: PIXI.Sprite;
    private _spinText: PIXI.Text;

    public boardWidth : number = GameScene.SYMBOL_WIDTH * GameScene.NUMBER_OF_REELS;
    public boardHeight : number = GameScene.SYMBOL_HEIGHT * GameScene.NUMBER_OF_ROWS;

    public init (): void {

        this.addChild(this._logoSprite);
        this._logoSprite.position.set(MainApp.SCREEN_WIDTH / 2, 100);
        this._logoSprite.anchor.set(0.5);
        this._logoSprite.scale.set(0.5);

        const style = new PIXI.TextStyle({
            fontFamily: 'Arial',
            fontSize: 36,
            fontWeight: 'bold',
            fill: ['#ffffff', '#00ff99'], // gradient
            stroke: '#4a1850',
            strokeThickness: 5,
            dropShadow: true,
            dropShadowColor: '#000000',
            dropShadowBlur: 4,
            dropShadowAngle: Math.PI / 6,
            dropShadowDistance: 6,
            wordWrap: true,
            wordWrapWidth: 440,
        });

        this._spinText = new PIXI.Text('Start Spin', style);
        this._spinText.x = MainApp.SCREEN_WIDTH / 2 - this._spinText.width / 2;
        this._spinText.y = MainApp.inst.app.screen.height - 100 + Math.round((100 - this._spinText.height) / 2);
        this.addChild(this._spinText);

        /**
         * Enable interactive so we can click on this text
         */
        this._spinText.interactive = true;
        this._spinText.buttonMode = true;
        this._spinText.addListener('pointerdown', this._startSpin.bind(this));


        //Init board
        this._slotBoard = new SlotBoard(this, GameScene.NUMBER_OF_REELS, GameScene.NUMBER_OF_ROWS, symbolTextures);  
        console.log('this._slotBoard',this._slotBoard);

        this._isInitialized = true;
    }

    public onUpdate (dtScalar: number) {
        const dt = dtScalar / PIXI.settings.TARGET_FPMS / 1000;
        if (this._isInitialized) {
            /**
             * Update objects in scene here using dt (delta time)
             * TODO: should call all update function of all the objects in Scene
             */
            this._logoSprite.rotation += 0.01;
        }
    }
    public getSymbolTextureByType(type: string){
        return symbolTextures[type]
    }
    public getRandomSymbolTexture(){
        return symbolTextures[symbolTypes[Utils.getRandomInt(symbolTypes.length)]];
    }
    public getRandomSymbolBlurTexture(){
        return symbolBlurTextures[symbolTypes[Utils.getRandomInt(symbolTypes.length)]];
    }

    private _startSpin (): void {
        this.disableSpinButton();
        console.log(` >>> start spin`);
        this._server.requestSpinData();
        this._slotBoard.startSpin();

    }

    private _onSpinDataResponded (data: string[]): void {
        console.log(` >>> received: ${data}`);
        this._slotBoard.endSpin(data, symbolTextures, ()=>{this.unableSpinButton()});
    }

    private _onAssetsLoaded (loaderInstance: PIXI.Loader, resources: Partial<Record<string, PIXI.LoaderResource>>): void {
        /**
         * After loading process is finished this function will be called
         */
        this._logoSprite = new PIXI.Sprite(resources['logo'].texture);
        symbolTypes.forEach((type) => {
            symbolTextures[type] = resources[`symbol_${type}`].texture;
        });

        MainApp.inst.app.loader
            .add('symbol_1_blur', 'images/symbol_1_blur.png')
            .add('symbol_2_blur', 'images/symbol_2_blur.png')
            .add('symbol_3_blur', 'images/symbol_3_blur.png')
            .add('symbol_4_blur', 'images/symbol_4_blur.png')
            .add('symbol_5_blur', 'images/symbol_5_blur.png')
            .add('symbol_6_blur', 'images/symbol_6_blur.png')
            .add('symbol_7_blur', 'images/symbol_7_blur.png')
            .add('symbol_8_blur', 'images/symbol_8_blur.png')
            .add('symbol_K_blur', 'images/symbol_K_blur.png')
            .load(this._onBlurAssetsLoaded.bind(this));   

        this.init();
    }
    private _onBlurAssetsLoaded(loaderInstance: PIXI.Loader, resources: Partial<Record<string, PIXI.LoaderResource>>): void{
        symbolTypes.forEach((type) => {
            symbolBlurTextures[type] = resources[`symbol_${type}_blur`].texture;
        });
        console.log('symbolBlurTextures',symbolBlurTextures);
    }
    private disableSpinButton(){
        this._spinText.alpha=0.5;
        this._spinText.interactive = false;
    }
    private unableSpinButton(){
        this._spinText.alpha=1;
        this._spinText.interactive = true;
    }
}